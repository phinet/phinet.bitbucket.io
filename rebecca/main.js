var init = () => {
    "use strict";

    var header = create_element({ tag: "h1", id: "header" });
    var content = create_element({ tag: "p" });

    var art = create_element({
        tag: "div",
        content: [
            { tag: "div", id: "top_colours",     class: "colours" },
            { tag: "div", id: "bottom_colour_a", class: "colours colour_box" },
            { tag: "div", id: "bottom_colour_b", class: "colours colour_box" },
            { tag: "div", id: "bottom_colour_c", class: "colours colour_box" },
        ]
    });

    document.body.appendChild(header);
    document.body.appendChild(content);
    document.body.appendChild(art);

    var hour = new Date().getHours();

    set_colours(hour);

    var header_text  = get_title(hour);
    var initial_text = "I thought I'd let you know that";
    var final_text   = get_cookie("message");

    var loaded_from_cookie = !!final_text;

    const save_cookie = () => {
        if (window.do_not_save_cookie) return;

        set_cookie("message", final_text, 900);
    }

    if (!loaded_from_cookie) {
        var lines;
        var date = new Date();

        try {
            lines = data.lines[date.getMonth()][date.getDate()];
        } catch (e) {}

        if (!lines) lines = (Math.random() < 0.1)? data.lines.often: data.lines.any;

        try {
            final_text = lines[Math.floor(lines.length * Math.random())];
        } catch (e) {
            final_text = "I love you (But my code might be broken)";
        }

        save_cookie();
    }

    var set_up_listeners = () => {
        setInterval(loop, 15 * 60 * 1000);

        window.addEventListener("focus", loop);

        window.addEventListener("unload", save_cookie);
    };

    var draw_text_slowely = () => {
        var write_header_text = () => {
            if (header_text.length > 0) {
                header.textContent += header_text.charAt(0);
                header_text = header_text.substring(1);
                setTimeout(write_header_text, 50);
            } else {
                header_text = header.textContent;
                setTimeout(write_initial_text, 500);
            }
        };

        var write_initial_text = () => {
            if (initial_text.length > 0) {
                content.textContent += initial_text.charAt(0);
                initial_text = initial_text.substring(1);
                setTimeout(write_initial_text, 50);
            } else {
                initial_text = content.textContent;
                setTimeout(clear_initial_text, 1000);
            }
        };

        var clear_initial_text = () => {
            var t = content.textContent;
            if (t.length > 0) {
                content.textContent = t.substring(0, t.length - 1);

                setTimeout(clear_initial_text, 25);
            } else {
                setTimeout(write_final_text, 500);
            }
        };

        var write_final_text = () => {
            if (final_text.length > 0) {
                content.textContent += final_text.charAt(0);
                final_text = final_text.substring(1);
                setTimeout(write_final_text, 50);
            } else {
                final_text = content.textContent;
                set_up_listeners();
            }
        };

        write_header_text();
    };

    var draw_text_quickly = () => {
        header.textContent = header_text;
        content.textContent = final_text;

        set_up_listeners();
    };

    var ready = (() => {
        var executed = false;

        return () => {
            if (executed) return;

            executed = true;

            ((loaded_from_cookie)? draw_text_quickly: draw_text_slowely)();
        };
    })();

    document.fonts.ready.then(ready);
    setTimeout(ready, 30 * 1000);

    snow();

    register_command("clear", () => {
        clear_cookie("message");

        window.do_not_save_cookie = true;

        location.reload();
    });
}

var snow = () => {
    const rand = window.random_table.create(10000);

    var canvas = create_element({
        tag: "canvas",
        style: `
            width: 100%;
            height: 100%;
            top: 0;
            left: 0;
            position: fixed;
            display: block;
            pointer-events: none;
            transition: opacity 0.25s;
        `
    });

    var context = canvas.getContext("2d");

    var flake, temp;

    var width, height, resize_timer, max_flakes, wind = 0;

    var flakes = [], swap = [];

    var set_resize_timer = () => {
        clearTimeout(resize_timer);

        resize_timer = setTimeout(set_canvas_size, 50);
    }

    var set_canvas_size = () => {
        canvas.width = width = window.innerWidth;
        canvas.height = height = window.innerHeight;
    }

    var update_timer;

    const update_storm = (x, y) => {
        if (!update_timer) {
            update_timer = setTimeout(() => {
                y = y * y + y / 2;

                const d = Math.sqrt(width * width + height * height);

                max_flakes = Math.floor(d * y);

                update_timer = null;
            }, 100);
        }
        
        wind = 2 * x - 1;
    }

    const pointer_move = (e) => {
        const x = Math.max(0, Math.min(1, e.clientX / width));
        const y = Math.max(0, Math.min(1, e.clientY / height));

        update_storm(x, y);
    }

    const create_flake = () => { return {
        x: (3 * rand.next() - 1) * width,
        y: rand.next() * height,
        v_x: wind * 0.5 * (rand.next() - 0.5),
        v_y: 2.5 * rand.next(),
        d: 1 + rand.next() * 4
    }};

    const init_storm = () => {
        limit_fps(false);

        if (running === true) return;

        clearTimeout(running);
        running = true;

        set_canvas_size();
        update_storm(rand.next(), rand.next());

        while (flakes.length < max_flakes) {
            flakes.push(create_flake());
        }

        canvas.style.opacity = 1;

        last_timestamp = -1;

        requestAnimationFrame(animation_loop);

        // window.requestAnimationFrame(step);
    }

    var running = false;

    document.body.appendChild(canvas);

    const hide = () => {
        canvas.style.opacity = 0;
        running = setTimeout(() => {
            running = false;
        }, 250);
    };

    const show = () => {
        init_storm();
    }

    var blur_timer;

    window.addEventListener("resize", set_resize_timer);
    window.addEventListener("mousemove", pointer_move);

    window.addEventListener("blur", () => {

        console.log("blur");
        limit_fps(20);

        blur_timer = setTimeout(() => {
            hide();
        }, 1000 * 60 * 15);
    });

    window.addEventListener("focus", () => {
        clearTimeout(blur_timer);
        show();
    });

    window.addEventListener("visibilitychange", () => {
        if (document.visibilityState === "visible") {
            show();
        } else {
            hide();
        }
    });

    let fps_last_timestamp, fps_target, fps_interval;

    const limit_fps = (target) => {
        if (target === false || target <= 0) target = -1;

        target_fps = target;
        fps_interval = 1000 / target;
    }

    const animation_loop = (timestamp) => {
        if (running !== false) requestAnimationFrame(animation_loop);

        const delta = (fps_last_timestamp >= 0)? timestamp - fps_last_timestamp: 0;

        if (target_fps > 0 && delta < fps_interval) return;

        fps_last_timestamp = timestamp - (delta % fps_interval);

        step(timestamp);
    }

    let last_timestamp;

    const step = (timestamp) => {
        const delta = (last_timestamp >= 0)? (timestamp - last_timestamp) / 1000: 0;
        last_timestamp = timestamp;

        const y_delta = 0.1 * delta;
        const x_delta = Math.sign(wind) * delta;
        const x_compare = (x_delta > 0)? Math.min: Math.max;

        while (flakes.length > 0) { // move flakes
            flake = flakes.pop();

            flake.y += height * flake.v_y * delta;
            flake.x += width * flake.v_x * delta;

            flake.v_y = Math.min(5, flake.v_y + y_delta);
            flake.v_x = x_compare(wind, flake.v_x + x_delta);

            if (flake.y < height) swap.push(flake);
        }

        while (swap.length < max_flakes) { // create new flakes
            flake = create_flake();
            flake.y -= height;

            swap.push(flake);
        }

        temp = flakes;
        flakes = swap;
        swap = temp;

        // draw

        const blur = 1 - Math.abs(wind) * 0.75;

        context.fillStyle = "rgba(0, 0, 0, " + blur + ")";
        context.globalCompositeOperation = "destination-out";

        context.fillRect(0, 0, width, height);

        context.fillStyle = "#fff";
        context.globalCompositeOperation = "source-over";

        for (i = flakes.length - 1; i >= 0; i--) {
            flake = flakes[i];

            context.fillRect(flake.x, flake.y, flake.d, flake.d);
        }

        // if (running !== false) window.requestAnimationFrame(step);
    };

    init_storm();
}

var get_title = (hour) => {
    "use strict";

    const times = [
        "morning", "morning", "afternoon", "evening"
    ];

    const time = times[Math.floor(hour / 6)];

    document.title = "Good " + time + "!";

    return "Good " + time;
}

var set_colours = (hour) => {
    "use strict";

    const top = document.getElementById("top_colours");
    const bottom_a = document.getElementById("bottom_colour_a");
    const bottom_b = document.getElementById("bottom_colour_b");

    const colours = [
        "#020115", "#000000", "#000000", "#070339", "#0c0568", "#16067b",
        "#f2671f", "#ffdb00", "#a9f1f6", "#6bcfff", "#4bc5ff", "#33beff",
        "#27b9ff", "#00adff", "#13b3ff", "#13aaff", "#0989ff", "#0968ff",
        "#f2671f", "#c91b26", "#9c0f5f", "#60047a", "#160a47", "#070339"
    ];

    const colour_a = colours[hour];
    const colour_b = colours[(hour + 1) % 24];

    bottom_a.style.borderColor = colour_a;
    bottom_b.style.borderColor = colour_b;
    top.style.background = "linear-gradient(to right, " + colour_a + ", " + colour_b + ")";
}

var loop = () => {
    "use strict";

    const hour = new Date().getHours();

    document.getElementById("header").textContent = get_title(hour);

    set_colours(hour);
}

document.addEventListener("DOMContentLoaded", init);
