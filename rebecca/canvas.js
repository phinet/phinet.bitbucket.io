(() => {

    const Canvas = function () {
        this.rand = window.random_table.create(10000);
        this.canvas = create_element({
            tag: "canvas",
            style: `
                width: 100%;
                height: 100%;
                top: 0;
                left: 0;
                position: fixed;
                display: block;
                pointer-events: none;
                transition: opacity 0.25s;
            `
        });
    }

    const create_canvas = () => {
        const canvas 

        const context = canvas.getContext("2d");

    var width, height, resize_timer;

    var set_resize_timer = () => {
        clearTimeout(resize_timer);

        resize_timer = setTimeout(set_canvas_size, 50);
    }

    var set_canvas_size = () => {
        canvas.width = width = window.innerWidth;
        canvas.height = height = window.innerHeight;
    }

    var update_timer;

    const update_storm = (x, y) => {
        if (!update_timer) {
            update_timer = setTimeout(() => {
                y = y * y + y / 2;

                const d = Math.sqrt(width * width + height * height);

                max_flakes = Math.floor(d * y);

                update_timer = null;
            }, 100);
        }
        
        wind = 2 * x - 1;
    }

    const pointer_move = (e) => {
        const x = Math.max(0, Math.min(1, e.clientX / width));
        const y = Math.max(0, Math.min(1, e.clientY / height));

        update_storm(x, y);
    }

    const create_flake = () => { return {
        x: (3 * rand.next() - 1) * width,
        y: rand.next() * height,
        v_x: wind * 0.5 * (rand.next() - 0.5),
        v_y: 2.5 * rand.next(),
        d: 1 + rand.next() * 4
    }};

    const init_storm = () => {
        limit_fps(false);

        if (running === true) return;

        clearTimeout(running);
        running = true;

        set_canvas_size();
        update_storm(rand.next(), rand.next());

        while (flakes.length < max_flakes) {
            flakes.push(create_flake());
        }

        canvas.style.opacity = 1;

        last_timestamp = -1;

        requestAnimationFrame(animation_loop);

        // window.requestAnimationFrame(step);
    }

    var running = false;

    document.body.appendChild(canvas);

    const hide = () => {
        canvas.style.opacity = 0;
        running = setTimeout(() => {
            running = false;
        }, 250);
    };

    const show = () => {
        init_storm();
    }

    var blur_timer;

    window.addEventListener("resize", set_resize_timer);
    window.addEventListener("mousemove", pointer_move);

    window.addEventListener("blur", () => {

        console.log("blur");
        limit_fps(20);

        blur_timer = setTimeout(() => {
            hide();
        }, 1000 * 60 * 15);
    });

    window.addEventListener("focus", () => {
        clearTimeout(blur_timer);
        show();
    });

    window.addEventListener("visibilitychange", () => {
        if (document.visibilityState === "visible") {
            show();
        } else {
            hide();
        }
    });

    let fps_last_timestamp, fps_target, fps_interval;

    const limit_fps = (target) => {
        if (target === false || target <= 0) target = -1;

        target_fps = target;
        fps_interval = 1000 / target;
    }

    const animation_loop = (timestamp) => {
        if (running !== false) requestAnimationFrame(animation_loop);

        const delta = (fps_last_timestamp >= 0)? timestamp - fps_last_timestamp: 0;

        if (target_fps > 0 && delta < fps_interval) return;

        fps_last_timestamp = timestamp - (delta % fps_interval);

        step(timestamp);
    }

    let last_timestamp;

    const step = (timestamp) => {
        const delta = (last_timestamp >= 0)? (timestamp - last_timestamp) / 1000: 0;
        last_timestamp = timestamp;

        const y_delta = 0.1 * delta;
        const x_delta = Math.sign(wind) * delta;
        const x_compare = (x_delta > 0)? Math.min: Math.max;

        while (flakes.length > 0) { // move flakes
            flake = flakes.pop();

            flake.y += height * flake.v_y * delta;
            flake.x += width * flake.v_x * delta;

            flake.v_y = Math.min(5, flake.v_y + y_delta);
            flake.v_x = x_compare(wind, flake.v_x + x_delta);

            if (flake.y < height) swap.push(flake);
        }

        while (swap.length < max_flakes) { // create new flakes
            flake = create_flake();
            flake.y -= height;

            swap.push(flake);
        }

        temp = flakes;
        flakes = swap;
        swap = temp;

        // draw

        const blur = 1 - Math.abs(wind) * 0.75;

        context.fillStyle = "rgba(0, 0, 0, " + blur + ")";
        context.globalCompositeOperation = "destination-out";

        context.fillRect(0, 0, width, height);

        context.fillStyle = "#fff";
        context.globalCompositeOperation = "source-over";

        for (i = flakes.length - 1; i >= 0; i--) {
            flake = flakes[i];

            context.fillRect(flake.x, flake.y, flake.d, flake.d);
        }

        // if (running !== false) window.requestAnimationFrame(step);
    };

    init_storm();
    }
})();
