(() => {
    "use strict";

    let ctx, map, body, dead, head, keys, move, rand, apple, score, buffer,
        canvas, map_ctx, running, highscore, max_score, block_size, map_canvas,
        apple_timer, block_count, block_space,buffer_canvas, last_direction;

    let timeouts;

    const draw = (c, x, y) => {
        c.fillRect(1 + x * (block_size + block_space), 1 + y * (block_size + block_space), block_size, block_size);
    };

    const clear = (c, x, y) => {
        c.clearRect(1 + x * (block_size + block_space), 1 + y * (block_size + block_space), block_size, block_size);
    };

    const draw_buffer  = (x, y) => draw(buffer, x, y);
    const draw_raw     = (x, y) => draw(ctx,    x, y);
    const clear_buffer = (x, y) => clear(buffer, x, y);
    const clear_raw    = (x, y) => clear(ctx,    x, y);

    const titlecard = (score) => {
        reset_game();

        ctx.clearRect(0, 0, canvas.width, canvas.height);
        ctx.drawImage(map_canvas, 0, 0);
        draw_raw(head.x, head.y);

        if (highscore > -1) {
            ctx.font = "10px Sans-Serif";
            ctx.fillText("Highscore: " + highscore, 5, 12);
        }

        if (score > -1) {
            ctx.font = "10px Sans-Serif";
            ctx.fillText("Score: " + score, 5, 27);
        }

        ctx.font = "20px Sans-Serif";
        ctx.fillText("Snake!", 5, canvas.height - 35);

        ctx.font = "10px Sans-Serif";
        ctx.fillText("Arrow keys to start :)", 5, canvas.height - 20);

        ctx.font = "10px Sans-Serif";
        ctx.fillText("1 = new map / 0 = clear", 5, canvas.height - 5);
    };

    const draw_game = () => {
        buffer.clearRect(0, 0, canvas.width, canvas.height);

        draw_buffer(head.x, head.y);

        if (apple) draw_buffer(apple.x, apple.y);

        for (const cell of body) {
            draw_buffer(cell.x, cell.y);
        }

        ctx.clearRect(0, 0, canvas.width, canvas.height);
        ctx.drawImage(map_canvas, 0, 0);
        ctx.drawImage(buffer_canvas, 0, 0);
    };

    const place_apple = () => {
        clearTimeout(apple_timer);

        for (let i = 0; i < 5; i++) {
            if (!apple) apple = {x: rand.next(), y: rand.next()};

            if (head.x === apple.x && head.y === apple.y) apple = undefined;
            if (apple && map && map[apple.x][apple.y]) apple = undefined;

            for (const cell of body) {
                if (!apple || cell.x === apple.x && cell.y === apple.y) {
                    apple = undefined;
                    break;
                }
            }
            if (apple) break;
        }

        if (!apple) {
            apple = {x: rand.next(), y: rand.next()};

            let collide;

            do {
                collide = false;

                if (head.x === apple.x && head.y === apple.y) collide = true;
                if (map && map[apple.x][apple.y]) collide = true;

                for (const cell of body) {
                    if (cell.x === apple.x && cell.y === apple.y) {
                        collide = true;
                        break;
                    }
                }

                if (!collide) break;

                head.x++;

                if (head.x >= block_count) {
                    head.x = 0;
                    head.y++;
                }

                if (head.y >= block_count) {
                    head.y = 0;
                }
            } while (collide);
        }

        apple_timer = setTimeout(() => {
            apple = undefined;

            place_apple();
        }, 10000);
    };

    const update_direction = () => {
        let key;

        if (!last_direction) {
            key = keys[0];
        } else if (keys.length > 1) {
            keys.shift();
            key = keys[0];
        }

        switch (key) {
        case "l":
            move.x = -1;
            move.y = 0;
            break;
        case "r":
            move.x = 1;
            move.y = 0;
            break;
        case "u":
            move.x = 0;
            move.y = -1;
            break;
        case "d":
            move.x = 0;
            move.y = 1;
            break;
        }

        if (key) last_direction = key;
    };

   const update_game = () => {
        let old_position = {x: head.x, y: head.y};

        head.x += move.x;
        head.y += move.y;

        if (head.x < 0 || head.x >= block_count) dead = true;
        if (head.y < 0 || head.y >= block_count) dead = true;
        if (!dead && map && map[head.x][head.y]) dead = true;

        for (const cell of body) {
            if (cell === body[0]) continue;

            if (cell.x === head.x && cell.y == head.y) dead = true;
        }

        if (!apple) place_apple();

        if (head.x === apple.x && head.y === apple.y) {
            apple = undefined;

            score++;

            body.unshift(body[0]);
        }

        body.push(old_position);
        body.shift();
    };

    const game_loop = (timestamp) => {
        let update;

        if (!move.next || move.next <= timestamp) {
            update_direction();
            update_game();

            move.next = timestamp + Math.max(0, 100 - 200 * score / max_score);

            update = true;
        }

        if (update) draw_game();

        if (dead) {
            if (!highscore || highscore < score) {
                highscore = score;
                set_cookie("snake_highscore", score, 3e7);
            }

            timeouts.push(setTimeout(() => clear_raw(head.x, head.y),  300));
            timeouts.push(setTimeout(() =>  draw_raw(head.x, head.y),  600));
            timeouts.push(setTimeout(() => clear_raw(head.x, head.y),  900));
            timeouts.push(setTimeout(() =>  draw_raw(head.x, head.y), 1200));
            timeouts.push(setTimeout(() => clear_raw(head.x, head.y), 1500));
            timeouts.push(setTimeout(() =>  draw_raw(head.x, head.y), 1800));
            timeouts.push(setTimeout(() => clear_raw(head.x, head.y), 2100));
            timeouts.push(setTimeout(() =>  draw_raw(head.x, head.y), 2400));
            timeouts.push(setTimeout(() => clear_raw(head.x, head.y), 2700));
            timeouts.push(setTimeout(() =>  draw_raw(head.x, head.y), 3000));

            timeouts.push(setTimeout(() => titlecard(score), 3300));
        } else {
            if (running) window.requestAnimationFrame(game_loop);
        }
    };

    const create_map = () => {
        map_ctx.clearRect(0, 0, canvas.width, canvas.height);

        if (map === false) return;

        map = [];

        for (let x = 0; x < block_count; x++) {
            let t = [];

            for (let y = 0; y < block_count; y++) {

                if (x >= 5 && x <= 19 && y >= 5 && y <= 19) {
                    t.push(false);
                } else {
                    t.push(Math.random() < 0.35);
                }
            }

            map.push(t);
        }

        const get = (x, y) => {
            return x < 0 || x >= block_count || y < 0 || y >= block_count || map[x][y];
        }

        const count = (x, y) => {
            return get(x - 1, y - 1) + get(x, y - 1) + get(x + 1, y - 1) + 
                   get(x - 1, y)     +                 get(x + 1, y)     + 
                   get(x - 1, y + 1) + get(x, y + 1) + get(x + 1, y + 1);
        }

        const automata = () => {
            let temp = [];

            for (let x = 0; x < block_count; x++) {
                temp.push([]);

                for (let y = 0; y < block_count; y++) {
                    if (map[x][y]) {
                        temp[x][y] = count(x, y) >= 3;
                    } else {
                        temp[x][y] = count(x, y) > 4;
                    }
                }
            }

            map = temp;
        }

        const narrows = () => {
            let temp = [];

            for (let x = 0; x < block_count; x++) {
                temp[x] = [];
                for (let y = 0; y < block_count; y++) {
                    temp[x][y] = map[x][y];
                }
            }

            for (let x = 0; x < block_count; x++) {
                for (let y = 0; y < block_count; y++) {
                    if (!temp[x][y]) {
                        if (get(x - 1, y) + get(x + 1, y) === 2) {
                            let t = (x > block_count / 2)? x - 1: x + 1;

                            temp[t][y] = false;
                        } 
                        if (get(x, y - 1) + get(x, y + 1) === 2) {
                            let t = (y > block_count / 2)? y - 1: y + 1;

                            temp[x][t] = false;
                        }
                    }
                }
            }

            map = temp;
        }

        for (let i = 0; i < 5; i++) {
            automata();
        }

        narrows();

        for (let i = 0; i < 2; i++) {
            automata();
            narrows();
        }

        for (let x = 0; x < block_count; x++) {
            for (let y = 0; y < block_count; y++) {
                if (map[x][y]) draw(map_ctx, x, y);
            }
        }
    };

    const reset_game = () => {
        move = {x: 0, y: 0};
        head = {x: 12, y: 12};
        score = 0;
        body = [];
        dead = false;
        keys = [];
        last_direction = undefined;
        timeouts = [];
        apple = undefined;

        clearTimeout(apple_timer);
        
        ctx.fillStyle = "white";
        buffer.fillStyle = "white";
        map_ctx.fillStyle = "#333";

        create_map();
    };

    const listener = (e) => {
        let start = move.y === 0 && move.x === 0;
        let last = (keys.length >= 0)? keys[keys.length - 1]: undefined;

        switch (e.key) {
        case "ArrowLeft":
            if (body.length === 0 || (last !== "l" && last !== "r")) keys.push("l");
            break;
        case "ArrowRight":
            if (body.length === 0 || (last !== "l" && last !== "r")) keys.push("r");
            break;
        case "ArrowUp":
            if (body.length === 0 || (last !== "u" && last !== "d")) keys.push("u");
            break;
        case "ArrowDown":
            if (body.length === 0 || (last !== "u" && last !== "d")) keys.push("d");
            break;
        case "1":
            if (start) {
                map = true;
                reset_game();
                titlecard();
            }
            break;
        case "0":
            if (start) {
                map = false;
                reset_game();
                titlecard();
            }
            break;
        }

        if (keys.length > 0 && start) {
            ctx.clearRect(0, 0, canvas.width, canvas.height);

            if (running) window.requestAnimationFrame(game_loop);
        }
    };

    const close_snake = () => {
        canvas.className = "";

        running = false;

        for (const i of timeouts) clearTimeout(i);

        window.removeEventListener("keydown", listener);
    }

    const start_snake = (c) => {
        running = true;

        canvas = c;
        ctx = canvas.getContext("2d");

        canvas.className = "crisp";

        block_count = 25;
        block_size = 4;
        block_space = 1;

        rand = random_table.create_int(1000, block_count);

        canvas.width = canvas.height = block_count * (block_size + block_space) + 1;

        max_score = block_count * block_count;

        buffer_canvas = document.createElement("canvas");
        buffer_canvas.width = canvas.width;
        buffer_canvas.height = canvas.height;
        buffer = buffer_canvas.getContext("2d");

        map_canvas = document.createElement("canvas");
        map_canvas.width = canvas.width;
        map_canvas.height = canvas.height;
        map_ctx = map_canvas.getContext("2d");

        highscore = parseInt(get_cookie("snake_highscore"));

        reset_game();

        titlecard();

        window.addEventListener("keydown", listener);
    };

    window.addEventListener("DOMContentLoaded", () => {
        window.register_game({ id: "snake", launch_text: "Play Snake :)", launch: start_snake, close: close_snake });
    });
})();