(() => {
    "use strict";

    const commands = {};

    const run = () => {
        const args = buffer.trim().split(/\s+/);

        const command = commands[args.shift()];

        buffer = "";

        if (command) return command.apply(null, args);
    };

    const register = (command, method) => {
        commands[command] = method;
    };

    const handler = (e) => {
        if (e.key === "Enter") return run(buffer);
        if (e.key.length === 1) buffer += e.key;

        console.log(buffer);
    };

    var buffer = "";

    window.register_command = register;
    window.addEventListener("keydown", handler);
})();