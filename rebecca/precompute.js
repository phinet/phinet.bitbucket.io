(function() {
"use strict";

var create = function(size, generator) {
    if (!generator) {
        generator = Math.random;
    }

    var data = {
        table: [],
        index: 0,
        size: size,
        loop: true,
        next: undefined
    }

    for (var i = size; i > 0; i--) {
        data.table.push(generator())
    }

    data.next = function() {
        if (data.index == data.size) {
            if (!data.loop) {
                for (var i = data.size - 1; i >= 0; i--) {
                    data.table[i] = generator();
                }
            }
            data.index = 0;
        }

        return data.table[data.index++];
    }

    return data;
}

var create_int = function(size, max) {
    var generator = function() {
        return Math.floor(Math.random() * max);
    }

    return create(size, generator);
}

// injection
window.random_table = {
    create:     create,
    create_int: create_int
};

})();