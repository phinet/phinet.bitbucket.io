(() => {
    "use strict";

    let canvas, ctx, running, board, can_move, score, highscore;

    const padding = 10;
    const radius = 20;
    const lookup = [];

    lookup[1 << 1]  = { y: 145, sz: 140, fg: "#000", bg: "#eee" };
    lookup[1 << 2]  = { y: 145, sz: 140, fg: "#000", bg: "#ffb968" };
    lookup[1 << 3]  = { y: 145, sz: 140, fg: "#000", bg: "#ffeb68" };
    lookup[1 << 4]  = { y: 135, sz: 115, fg: "#000", bg: "#edff68" };
    lookup[1 << 5]  = { y: 135, sz: 115, fg: "#000", bg: "#c4ff68" };
    lookup[1 << 6]  = { y: 135, sz: 115, fg: "#000", bg: "#95ff68" };
    lookup[1 << 7]  = { y: 127, sz:  85, fg: "#000", bg: "#68ff81" };
    lookup[1 << 8]  = { y: 127, sz:  85, fg: "#000", bg: "#68ffac" };
    lookup[1 << 9]  = { y: 127, sz:  85, fg: "#000", bg: "#68ffe7" };
    lookup[1 << 10] = { y: 120, sz:  67, fg: "#000", bg: "#68dbff" };
    lookup[1 << 11] = { y: 120, sz:  67, fg: "#000", bg: "#68a5ff" };
    lookup[1 << 12] = { y: 120, sz:  67, fg: "#000", bg: "#6d68ff" };
    lookup[1 << 13] = { y: 120, sz:  67, fg: "#000", bg: "#b168ff" };
    lookup[1 << 14] = { y: 115, sz:  54, fg: "#000", bg: "#f468ff" };
    lookup[1 << 15] = { y: 115, sz:  54, fg: "#000", bg: "#ff68c5" };
    lookup[1 << 16] = { y: 115, sz:  54, fg: "#000", bg: "#ff6875" };

    function draw_square(x, y, w, h, radius) {
        let r = x + w;
        let b = y + h;

        ctx.beginPath();

        ctx.moveTo(x+radius, y);
        ctx.lineTo(r-radius, y);
        ctx.quadraticCurveTo(r, y, r, y+radius);
        ctx.lineTo(r, y+h-radius);
        ctx.quadraticCurveTo(r, b, r-radius, b);
        ctx.lineTo(x+radius, b);
        ctx.quadraticCurveTo(x, b, x, b-radius);
        ctx.lineTo(x, y+radius);
        ctx.quadraticCurveTo(x, y, x+radius, y);

        ctx.fill();
    }

    const draw_block = (block) => {
        const data = lookup[block.number];

        ctx.fillStyle = data.bg;

        const dimensions = 200 - padding * 2;

        draw_square(block.x + padding, block.y + padding, dimensions, dimensions, radius);

        ctx.fillStyle = data.fg;
        ctx.font = "bold " + data.sz + "px Sans-Serif";
        ctx.textAlign = "center"; 

        ctx.fillText(block.number, block.x + 100, block.y + data.y + padding);
    };

    const draw_loop = () => {
        ctx.clearRect(0, 0, canvas.width, canvas.height);

        for (let x = 0; x < 4; x++) {
            for (let y = 0; y < 4; y++) {
                let b = board[x][y];

                if (!b) continue;

                if (typeof b.target_x === "number") {
                    b.x = Math.floor((b.x + b.target_x) / 2);

                    if (Math.abs(b.x - b.target_x) < 0.01) b.target_x = undefined;
                }

                if (typeof b.target_y === "number") {
                    b.y = Math.floor((b.y + b.target_y) / 2);

                    if (Math.abs(b.y - b.target_y) < 0.01) b.target_y = undefined;
                }

                draw_block(b);
            }
        }

        if (running) {
            window.requestAnimationFrame(draw_loop);
        } else {
            setTimeout(display_end, 1000);
        }
    };

    const display_title = () => {
        reset_game();

        ctx.fillStyle = "white";
        ctx.textAlign = "start";

        if (highscore) {
            ctx.font = "bold 50px Sans-Serif";
            ctx.fillText("Highscore: " + highscore, 25, 75);
        }

        ctx.font = "bold 200px Sans-Serif";
        ctx.fillText("2048", 25, 700);

        ctx.font = "bold 50px Sans-Serif";
        ctx.fillText("Press space to start :)", 25, 775);
    };

    const display_end = () => {
        ctx.fillStyle = "rgba(255, 255, 255, 0.75)";

        draw_square(75, 175, 650, 450, 50);

        ctx.fillStyle = "black";
        ctx.textAlign = "center"; 

        ctx.font = "bold 80px Sans-Serif";

        if (score > highscore) {
            ctx.fillText("New Highscore!", 400, 300);
        } else {
            ctx.fillText("Game Over", 400, 300);
        }
        
        ctx.font = "bold 60px Sans-Serif";

        ctx.fillText("Score: " + score, 400, 425);

        ctx.font = "bold 40px Sans-Serif";

        ctx.fillText("Space to restart :)", 400, 550);
    };

    const check = () => {
        for (let x = 0; x < 4; x++) {
            for (let y = 0; y < 4; y++) {
                let a = board[x][y];

                if (!a) return true;

                if (x < 3) {
                    let b = board[x+1][y];

                    if (!b || a.number === b.number) return true;
                }

                if (y < 3) {
                    let b = board[x][y+1];

                    if (!b || a.number === b.number) return true;
                }
            }
        }

        return false;
    }

    const place_block = () => {
        board.count++;
        score += 2;
 
        let placed;

        for (let i = 0; !placed && i < 16; i++) {
            let x = Math.floor(Math.random() * 4);
            let y = Math.floor(Math.random() * 4);

            if (!board[x][y]) {
                board[x][y] = {x: x * 200, y: y * 200, number: 2 };
                placed = true;
            }
        }

        for (let x = 0; !placed && x < 4; x++) {
            for (let y = 0; !placed && y < 4; y++) {
                if (!board[x][y]) {
                    board[x][y] = {x: x * 200, y: y * 200, number: 2 };
                    placed = true;
                }
            }
        }

        if (board.count >= 16 && !check()) {
            running = false;

            highscore = highscore || 0;

            if (score > highscore) set_cookie("2048_highscore", score, 3e7);
        }
    };

    const slide = (x_1, y_1, x_2, y_2) => {
        if (board[x_1][y_1]) {
            board.count--;
            score += board[x_2][y_2].number;
            board[x_2][y_2].number *= 2;
        }

        board[x_1][y_1] = board[x_2][y_2];
        board[x_2][y_2] = undefined;

        let next = board[x_1][y_1];

        next.target_x = x_1 * 200;
        next.target_y = y_1 * 200;
    }

    const move = (direction) => {
        if (!can_move) return;

        let moved;

        switch (direction) {
        case "left":
            for (let y = 0; y < 4; y++) {
                for (let x = 0; x < 4; x++) {
                    if (!board[x][y]) {
                        for (let next = x + 1; next < 4; next++) {
                            if (board[next][y]) {
                                slide(x, y, next, y);
                                moved = true;
                                x--;
                                break;
                            }
                        }
                    } else {
                        for (let next = x + 1; next < 4; next++) {
                            if (board[next][y]) {
                                if (board[next][y].number === board[x][y].number) {
                                    slide(x, y, next, y);
                                    moved = true;
                                }
                                break;
                            }
                        }
                    }
                }
            }
            break;
        case "right":
            for (let y = 0; y < 4; y++) {
                for (let x = 3; x >= 0; x--) {
                    if (!board[x][y]) {
                        for (let next = x - 1; next >= 0; next--) {
                            if (board[next][y]) {
                                slide(x, y, next, y);
                                moved = true;
                                x++;
                                break;
                            }
                        }
                    } else {
                        for (let next = x - 1; next >= 0; next--) {
                            if (board[next][y]) {
                                if (board[next][y].number === board[x][y].number) {
                                    slide(x, y, next, y);
                                    moved = true;
                                }
                                break;
                            }
                        }
                    }
                }
            }
            break;
        case "up":
            for (let x = 0; x < 4; x++) {
                for (let y = 0; y < 4; y++) {
                    if (!board[x][y]) {
                        for (let next = y + 1; next < 4; next++) {
                            if (board[x][next]) {
                                slide(x, y, x, next);
                                moved = true;
                                y--;
                                break;
                            }
                        }
                    } else {
                        for (let next = y + 1; next < 4; next++) {
                            if (board[x][next]) {
                                if (board[x][next].number === board[x][y].number) {
                                    slide(x, y, x, next);
                                    moved = true;
                                }
                                break;
                            }
                        }
                    }
                }
            }
            break;
        case "down":
            for (let x = 0; x < 4; x++) {
                for (let y = 3; y >= 0; y--) {
                    if (!board[x][y]) {
                        for (let next = y - 1; next >= 0; next--) {
                            if (board[x][next]) {
                                slide(x, y, x, next);
                                moved = true;
                                y++
                                break;
                            }
                        }
                    } else {
                        for (let next = y - 1; next >= 0; next--) {
                            if (board[x][next]) {
                                if (board[x][next].number === board[x][y].number) {
                                    slide(x, y, x, next);
                                    moved = true;
                                }
                                break;
                            }
                        }
                    }
                }
            }
            break;
        }

        if (moved) {
            can_move = false;

            setTimeout(() => {
                place_block();
                can_move = true;
            }, 100);
        }
    };

    const listener = (e) => {
        let direction;

        switch (e.key) {
        case "ArrowLeft":  direction =  "left"; break;
        case "ArrowRight": direction = "right"; break;
        case "ArrowUp":    direction =    "up"; break;
        case "ArrowDown":  direction =  "down"; break;
        case " ":
            if (!running) {
                running = true;

                reset_game();

                place_block();

                window.requestAnimationFrame(draw_loop);
            }
            break;
        }

        if (running && direction) move(direction);
    };

    const reset_game = () => {
        board = [[],[],[],[]];
        board.count = 0;
        can_move = true;
        score = 0;
        highscore = parseInt(get_cookie("2048_highscore"));
    };

    const close_2048 = () => {
        window.removeEventListener("keydown", listener);
        running = false;
    };

    const start_2048 = (c) => {
        canvas = c;

        canvas.width = canvas.height = 800;

        ctx = canvas.getContext("2d");

        display_title();

        window.addEventListener("keydown", listener);
    };

    window.addEventListener("DOMContentLoaded", () => {
        window.register_game({ id: "2048", launch_text: "Play 2048", launch: start_2048, close: close_2048 });
    });
})();