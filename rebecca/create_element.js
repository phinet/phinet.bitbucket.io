var create_element = window.create_element = function (params) {
    "use strict";

    var cases = {
        string: (text) => {
            return document.createTextNode(text);
        },
        object: (params) => {
            // get params

            var tag = params.tag;
            var events = params.events;
            var content = params.content;

            delete params.tag;
            delete params.events;
            delete params.content;

            // create element

            var element = document.createElement(tag);

            // add events

            for (var type in events) {
                var list = events[type];

                if (!Array.isArray(list)) list = [list];

                for (const event of list) {
                    element.addEventListener(type, event);
                }
            }

            // add children

            if (typeof content === "undefined") content = [];

            if (!Array.isArray(content)) content = [content];

            for (const val of content) {
                element.appendChild(create_element(val));
            }

            // add params

            for (var key in params) {
                var value = params[key];

                if (typeof value === "string") {
                    element.setAttribute(key, params[key]);
                } else {
                    element[key] = params[key];
                }
            }

            return element;
        },
        node: (node) => {
            return node;
        }
    };

    var type = (params instanceof HTMLElement)? "node": typeof params;

    var handler = cases[type];

    if (!handler) {
        throw ("Undefined handler for type " + type);
    }

    return handler(params);
};