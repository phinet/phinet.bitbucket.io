(() => {
    "use strict";

    const parse_path = (path) => {
        if (typeof path !== "string") return ";path=/";

        return ";path=" + path;
    }

    const parse_age = (age) => {
        if (typeof age !== "number") return "";

        if (age < 0) return ";Expires=Thu, 01 Jan 1970 00:00:01 GMT";

        return ";max-age=" + Math.floor(age);
    }

    const create = (key, value, max_age, path) => key + "=" + value + parse_age(max_age) + parse_path(path)

    const set = (key, value, max_age, path) => document.cookie = create(key, value, max_age, path);

    const clear = (key, path) => set(key, "", 0, path);

    const get = (key) => {
        const prefix = key + "=";
        const cookies = document.cookie.split(';');

        for (var cookie of cookies) {
            cookie = cookie.trim();

            if (cookie.startsWith(prefix)) {
                return cookie.substring(prefix.length, cookie.length);
            }
        }

        return null;
    };

    window.set_cookie = set;
    window.get_cookie = get;
    window.clear_cookie = clear;
})();

