(() => {
    "use strict";

    const manager = {};

    const init_game_manager = (e) => {
        let resizer_size, mouse_move, mouse_move_x, mouse_move_y;

        manager.launcher = create_element({ tag: "div", id: "game_launcher" });

        manager.canvas = create_element({ tag: "canvas" });
        manager.resizer = create_element({ tag: "div", content: manager.canvas });
        manager.close = create_element({
            events: { click: close_game },
            tag: "a", content: "🗙", href: "#"
        });
        manager.container = create_element({
            tag: "div",
            id: "game_container",
            content: [ manager.close, manager.resizer ]
        });

        const update_position = (x, y) => {
            set_cookie("game_x", x, 3e7);
            set_cookie("game_y", y, 3e7);

            manager.resizer.style.left = x + "px";
            manager.resizer.style.top = y + "px";
        };

        const update_size = (size) => {
            set_cookie("game_size", size, 3e7);

            const old_width = manager.canvas.offsetWidth;
            const old_height = manager.canvas.offsetHeight;

            manager.resizer.style.width = manager.resizer.style.height = size + "px";

            const width_change = old_width - manager.canvas.offsetWidth;
            const height_change = old_height - manager.canvas.offsetHeight;

            update_position(
                manager.resizer.offsetLeft + width_change / 2,
                manager.resizer.offsetTop + height_change / 2
            );
        };

        let cookie_x = parseInt(get_cookie("game_x")) || 100;
        let cookie_y = parseInt(get_cookie("game_y")) || 100;
        let cookie_size = parseInt(get_cookie("game_size")) || 250;

        if (cookie_size) {
            cookie_size = Math.max(0, Math.min(cookie_size, window.innerWidth, window.innerHeight));

            manager.resizer.style.width = manager.resizer.style.height = cookie_size + "px";
        }

        if (cookie_x) {
            let max = window.innerWidth - cookie_size - (manager.resizer.offsetWidth - manager.canvas.offsetWidth);

            cookie_x = Math.max(-1, Math.min(cookie_x, max));

            manager.resizer.style.left = cookie_x + "px";
        }

        if (cookie_y) {
            let max = window.innerHeight - cookie_size - (manager.resizer.offsetHeight - manager.canvas.offsetHeight);

            cookie_y = Math.max(-1, Math.min(cookie_y, max));

            manager.resizer.style.top = cookie_y + "px";
        }

        manager.resizer.addEventListener("mouseup", () => {
            if (resizer_size !== manager.canvas.offsetWidth || resizer_size !== manager.canvas.offsetHeight) {
                resizer_size = Math.min(manager.canvas.offsetWidth, manager.canvas.offsetHeight);

                update_size(resizer_size);
            }
        });

        manager.resizer.addEventListener("dblclick", () => {
            let min = Math.min(window.innerHeight, window.innerWidth);
            let target = manager.canvas.height;

            if (manager.canvas.offsetHeight !== target) {
                for (let i = 1; i < 10; i++) {
                    if (target / i < min) {
                        update_size(target / i);
                        return;
                    }
                }
            } else {
                let size = target;

                for (let i = 1; i < 10; i++) {
                    if (target * i >= min) break;
                    size = target * i;
                }

                update_size(size);
            }
        });

        manager.canvas.addEventListener("mousedown", (e) => {
            mouse_move = true;

            mouse_move_x = manager.resizer.offsetLeft - e.clientX;
            mouse_move_y = manager.resizer.offsetTop - e.clientY;
        });

        window.addEventListener("mouseup", (e) => mouse_move = false);

        window.addEventListener("mousemove", (e) => {
            if (mouse_move) {
                update_position(e.clientX + mouse_move_x, e.clientY + mouse_move_y);
            }
        });

        document.body.appendChild(manager.launcher);
        document.body.appendChild(manager.container);

        window.register_game = register_game;
    };

    const close_game = (e) => {
        e.preventDefault();

        manager.launcher.style.display = "block";
        manager.container.style.display = "none";

        manager.game.close();

        return false;
    };

    const launch_game = () => {
        manager.launcher.style.display = "none";
        manager.container.style.display = "block";

        manager.game.launch(manager.canvas);
    };

    const register_game = (game) => {
        const launcher = create_element({
            tag: "a",
            href: "#",
            events: {
                click: (e) => { 
                    e.preventDefault();

                    manager.game = game;

                    launch_game();
                    
                    return false;
                }
            },
            content: game.launch_text
        });

        manager.launcher.appendChild(launcher);
    };

    window.addEventListener("DOMContentLoaded", init_game_manager);
})();